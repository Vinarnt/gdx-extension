/**
 * Copyright 2013
 * <p/>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p/>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p/>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.gdx.extension;

import com.badlogic.gdx.scenes.scene2d.ui.Skin;
import com.badlogic.gdx.scenes.scene2d.ui.Table;
import com.badlogic.gdx.utils.Array;
import com.gdx.extension.event.ScreenListener;

/**
 * Create an empty screen with a layout table expanding the whole stage viewport.
 * Add your actors to the layout to automatically handle the screen add/remove/show/hide actions.
 */
public class BaseScreen implements Comparable<BaseScreen> {

    /**
     * Style
     */
    protected Skin skin;

    /**
     * Internal screen layout
     */
    protected Table layout;

    /**
     * If true, rendered by the {@link ScreenManager}
     */
    protected boolean isActive;

    /**
     * Depth of the screen
     */
    protected int depth;

    /**
     * The {@link ScreenManager screen manager} of this screen
     */
    protected ScreenManager screenManager;

    protected Array<ScreenListener> listeners;

    /**
     * Create a new screen.
     *
     * @param screenManager the {@link ScreenManager} who's adding the {@link BaseScreen}
     * @param depth the depth you want screen to be rendered
     */
    public BaseScreen(ScreenManager screenManager, int depth) {
        this.screenManager = screenManager;

        skin = screenManager.getSkin();
        listeners = new Array<ScreenListener>();

        layout = new Table();
        setDepth(depth);
    }

    public void render(float delta) {
    }

    public void resize(int width, int height) {
    }

    /**
     * Called internally when the screen is showed.
     */
    protected void show() {
    }

    /**
     * Called internally when the screen is hidden.
     */
    protected void hide() {
    }

    /**
     * Called when the screen is paused (on android).
     */
    public void pause() {
    }

    /**
     * Called when the screen is resumed (on android).
     */
    public void resume() {
    }

    /**
     * Called when the screen need to be disposed.
     */
    public void dispose() {
    }

    /**
     * @return if screen is rendered
     */
    public boolean isActive() {
        return isActive;
    }

    /**
     * @param isActive rendered or not
     */
    public void setActive(boolean isActive) {
        if (this.isActive == isActive) {
            return;
        }

        this.isActive = isActive;

        screenManager.updateScreens();
    }

    /**
     * @return the screen depth
     */
    public int getDepth() {
        return depth;
    }

    /**
     * Set the depth of the screen.
     *
     * @param depth screen depth
     */
    public void setDepth(int depth) {
        this.depth = depth;

        screenManager.updateScreens();
    }

    /**
     * Get the screen manager of this screen.
     *
     * @return the screen manager
     */
    public ScreenManager getScreenManager() {
        return screenManager;
    }

    /**
     * Add a listener to this screen.
     *
     * @param listener listener to add
     */
    public void addListener(ScreenListener listener) {
        listeners.add(listener);
    }

    /**
     * Remove a listener from this screen.
     *
     * @param listener listener to remove
     */
    public void removeListener(ScreenListener listener) {
        listeners.removeValue(listener, true);
    }

    /**
     * @return all the listeners of this screen
     */
    public Array<ScreenListener> getListeners() {
        return listeners;
    }

    /**
     * Used internally to sort screen by depth.
     */
    public int compareTo(BaseScreen c) {
        int value = 0;
        if (depth > c.depth) {
            value = 1;
        } else if (depth < c.depth) {
            value = -1;
        } else if (depth == c.depth) {
            value = 0;
        }

        return value;
    }
}