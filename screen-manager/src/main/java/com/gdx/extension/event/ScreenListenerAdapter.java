package com.gdx.extension.event;

import com.gdx.extension.BaseScreen;
import com.gdx.extension.ScreenManager;

public class ScreenListenerAdapter implements ScreenListener {

    public void onRegister(ScreenManager screenManager, BaseScreen screen) {
    }

    public void onShow(ScreenManager screenManager, BaseScreen screen) {
    }

    public void onHide(ScreenManager screenManager, BaseScreen screen) {
    }

    public void onUnregister(ScreenManager screenManager, BaseScreen screen) {
    }
}