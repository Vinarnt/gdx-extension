/**
 * Copyright 2013
 * <p/>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p/>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p/>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.gdx.extension.grid;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Input.Buttons;
import com.badlogic.gdx.Input.Keys;
import com.badlogic.gdx.graphics.g2d.Batch;
import com.badlogic.gdx.scenes.scene2d.Actor;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.InputListener;
import com.badlogic.gdx.scenes.scene2d.Touchable;
import com.badlogic.gdx.scenes.scene2d.ui.Container;
import com.badlogic.gdx.scenes.scene2d.ui.Skin;
import com.badlogic.gdx.scenes.scene2d.utils.ChangeListener.ChangeEvent;
import com.badlogic.gdx.scenes.scene2d.utils.Drawable;
import com.badlogic.gdx.utils.Pools;
import com.gdx.extension.SelectionMode;
import com.gdx.extension.group.ActorGroup;
import com.gdx.extension.group.Groupable;

/**
 * It's a selectable item from a {@link GridSelection}.
 * You can define a custom item view by extending it and add actors to it.
 * It handle multi selection
 *
 * @author Kyu
 */
public class GridSelectionItem extends Container<Actor> implements Groupable {

    private ActorGroup<GridSelectionItem> gridItemGroup;
    private boolean isDisabled, isChecked, isOver;
    private GridSelectionItemStyle style;
    private GridSelection<? extends GridSelectionItem> grid;

    /**
     * Create an {@link GridSelectionItem item} with default style.
     *
     * @param skin the {@link Skin skin} to use
     */
    public GridSelectionItem(Skin skin) {
        this(skin, "default");
    }

    /**
     * Create an {@link GridSelectionItem item} with the specified style.
     *
     * @param skin      the {@link Skin skin} to use
     * @param styleName the {@link GridSelectionItemStyle style} to use
     */
    public GridSelectionItem(Skin skin, String styleName) {
        this(skin.get(styleName, GridSelectionItemStyle.class));
    }

    public GridSelectionItem(GridSelectionItemStyle style) {
        setTransform(false);
        setTouchable(Touchable.enabled);

        setStyle(style);
    }

    /**
     * @return the preferred width
     */
    @Override
    public float getPrefWidth() {
        return getWidth();
    }

    /**
     * @return the preferred height
     */
    @Override
    public float getPrefHeight() {
        return getHeight();
    }

    /**
     * Draw the background of the {@link GridSelectionItem item}
     */
    @Override
    public void draw(Batch batch, float parentAlpha) {
        super.draw(batch, parentAlpha);

        Drawable background = null;
        if (style.background != null && isDisabled) {
            background = style.disabled;
        } else if (style.checked != null && isChecked) {
            background = style.checked;
        } else if (style.over != null && isOver) {
            background = style.over;
        } else if (style.background != null) {
            background = style.background;
        }

        setBackground(background);
    }

    /**
     * Apply the style.
     *
     * @param style the {@link GridSelectionItemStyle style} to apply
     */
    public void setStyle(GridSelectionItemStyle style) {
        this.style = style;
    }

    /**
     * @return the parent group
     */
    @Override
    public ActorGroup<? extends Groupable> getActorGroup() {
        return gridItemGroup;
    }

    /**
     * Used internally.
     */
    @SuppressWarnings("unchecked")
    @Override
    public void setActorGroup(ActorGroup<? extends Groupable> group) {
        gridItemGroup = (ActorGroup<GridSelectionItem>) group;
    }

    /**
     * @return if {@link GridSelectionItem item} is checked
     */
    @Override
    public boolean isChecked() {
        return isChecked;
    }

    /**
     * Set the checked state of the {@link GridSelectionItem item}.
     *
     * @param isChecked the checked state
     */
    @Override
    public void setChecked(boolean isChecked) {
        if (this.isChecked == isChecked) {
            return;
        }

        if (gridItemGroup != null && !gridItemGroup.canCheck(this, isChecked)) {
            return;
        }

        this.isChecked = isChecked;

        ChangeEvent changeEvent = Pools.obtain(ChangeEvent.class);
        if (fire(changeEvent)) {
            this.isChecked = !isChecked;
        }

        Pools.free(changeEvent);
    }

    /**
     * @return if the {@link GridSelectionItem item} is disable
     */
    @Override
    public boolean isDisabled() {
        return isDisabled;
    }

    /**
     * Set the disabled state of the {@link GridSelectionItem item}.
     *
     * @param isDisabled the disabled state
     */
    @Override
    public void setDisabled(boolean isDisabled) {
        this.isDisabled = isDisabled;
    }

    /**
     * Used internally.
     *
     * @param grid the parent grid
     */
    public <T> void setGrid(final GridSelection<? extends GridSelectionItem> grid) {
        this.grid = grid;

        addListener(new InputListener() {

            @Override
            public boolean touchDown(InputEvent event, float x, float y, int pointer, int button) {
                if (button != Buttons.LEFT || !grid.isSelectable()) {
                    return false;
                }

                if (grid.getSelectionMode() == SelectionMode.MULTI) {
                    if (!gridItemGroup.isMultiSelection()) {
                        if (gridItemGroup.isMultiOnCtrl()) {
                            if (Gdx.input.isKeyPressed(Keys.CONTROL_LEFT) || Gdx.input
                                    .isKeyPressed(Keys.CONTROL_RIGHT)) {
                                gridItemGroup.beginSelection();
                            }
                        } else {
                            gridItemGroup.beginSelection();
                        }
                    } else {
                        if (gridItemGroup.isMultiOnCtrl()) {
                            if (!Gdx.input.isKeyPressed(Keys.CONTROL_LEFT) && !Gdx.input
                                    .isKeyPressed(Keys.CONTROL_RIGHT)) {
                                gridItemGroup.endSelection();
                            }
                        }
                    }
                }

                if (gridItemGroup.isMultiSelection() && isChecked()) {
                    gridItemGroup.uncheck(GridSelectionItem.this);
                } else {
                    gridItemGroup.setChecked(GridSelectionItem.this);
                }

                return true;
            }

            @Override
            public void enter(InputEvent event, float x, float y, int pointer, Actor fromActor) {
                isOver = true;
            }

            @Override
            public void exit(InputEvent event, float x, float y, int pointer, Actor toActor) {
                isOver = false;
            }
        });
    }

    /**
     * Style class
     *
     * @author Kyu
     */
    public static class GridSelectionItemStyle {

        /**
         * Optional
         */
        public Drawable background, disabled, checked, over;

        public GridSelectionItemStyle() {
        }

    }
}