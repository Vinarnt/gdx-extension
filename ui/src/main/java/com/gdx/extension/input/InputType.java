package com.gdx.extension.input;

public enum InputType {
    Mouse, Keyboard
}